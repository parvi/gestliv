import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { EncoursTrajetPage } from './encours-trajet.page';

const routes: Routes = [
  {
    path: '',
    component: EncoursTrajetPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [EncoursTrajetPage]
})
export class EncoursTrajetPageModule {}
