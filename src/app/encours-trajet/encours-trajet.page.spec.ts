import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EncoursTrajetPage } from './encours-trajet.page';

describe('EncoursTrajetPage', () => {
  let component: EncoursTrajetPage;
  let fixture: ComponentFixture<EncoursTrajetPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EncoursTrajetPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EncoursTrajetPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
