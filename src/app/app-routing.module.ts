import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
 /* {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },*/
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then(m => m.HomePageModule)
  },
  {
    path: 'list',
    loadChildren: () => import('./list/list.module').then(m => m.ListPageModule)
  },
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
  { path: 'add-client', loadChildren: './add-client/add-client.module#AddClientPageModule' },
  { path: 'add-livreur', loadChildren: './add-livreur/add-livreur.module#AddLivreurPageModule' },
  { path: 'add-course', loadChildren: './add-course/add-course.module#AddCoursePageModule' },
  { path: 'commandes', loadChildren: './commandes/commandes.module#CommandesPageModule' },
  { path: 'courses', loadChildren: './courses/courses.module#CoursesPageModule' },
  { path: 'tracker-test', loadChildren: './tracker-test/tracker-test.module#TrackerTestPageModule' },  { path: 'choisir-trajet', loadChildren: './choisir-trajet/choisir-trajet.module#ChoisirTrajetPageModule' },
  { path: 'encours-trajet', loadChildren: './encours-trajet/encours-trajet.module#EncoursTrajetPageModule' },
  { path: 'connexion', loadChildren: './connexion/connexion.module#ConnexionPageModule' }

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
