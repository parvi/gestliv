import { TestBed } from '@angular/core/testing';

import { LoadingctrlService } from './loadingctrl.service';

describe('LoadingctrlService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LoadingctrlService = TestBed.get(LoadingctrlService);
    expect(service).toBeTruthy();
  });
});
