import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class LoadingctrlService {
  loader:any
  constructor(public loadingController: LoadingController){
    //creates loader once so there is less data redundancy
  }
/*  showLoader() { //call this fn to show loader
      this.loader = this.loadingCtrl.create({
          content: `loading...`,
      });
      this.loader.present(); 
      return this.loader;   
  }*/
  async presentLoading() {
    const loading = await this.loadingController.create({
      message: 'chargement...',
      duration: 2000
    });
    await loading.present();
    return loading;
    //const { role, data } = await loading.onDidDismiss();

  }

}
