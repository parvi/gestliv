import { Component, OnInit } from '@angular/core';
import { TrajetService } from '../api/trajet.service';

@Component({
  selector: 'app-list',
  templateUrl: 'list.page.html',
  styleUrls: ['list.page.scss']
})
export class ListPage implements OnInit {
  private selectedItem: any;
  private icons = [
    'flask',
    'wifi',
    'beer',
    'football',
    'basketball',
    'paper-plane',
    'american-football',
    'boat',
    'bluetooth',
    'build'
  ];
  public items: Array<{ title: string; note: string; icon: string }> = [];
  trajet:any =[];
  constructor(public servicesTrajet: TrajetService) {
    this.mesTrajets();
    for (let i = 1; i < 11; i++) {
      this.items.push({
        title: 'Item ' + i,
        note: 'This is item #' + i,
        icon: this.icons[Math.floor(Math.random() * this.icons.length)]
      });
    }
  }

  ngOnInit() {
  }
  // add back when alpha.4 is out
  // navigate(item) {
  //   this.router.navigate(['/list', JSON.stringify(item)]);
  // }
  mesTrajets(){
    console.log("suis la")
    this.servicesTrajet.trajetAlivrer()
    .subscribe(reponse=>{
      this.trajet = reponse;
      console.log(this.trajet);
    })
  }
}
