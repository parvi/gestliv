import { Component } from '@angular/core';
import { UsersService } from '../api/users.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(public services: UsersService, public storage: Storage) {}
   

  getUserInfo(){
    this.storage.get('user').then(user=>{
        if(user){
          this.storage.get('identifiants').then(identifiants=>{
            this.services.GetUserInfo(identifiants).subscribe(livreur=>{
              if(livreur){
                this.storage.set('user',livreur);
              }
            })
          })
        }
    })
  }
}
