import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrackerTestPage } from './tracker-test.page';

describe('TrackerTestPage', () => {
  let component: TrackerTestPage;
  let fixture: ComponentFixture<TrackerTestPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrackerTestPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrackerTestPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
