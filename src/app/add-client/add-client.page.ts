import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { UsersService } from '../api/users.service';

@Component({
  selector: 'app-add-client',
  templateUrl: './add-client.page.html',
  styleUrls: ['./add-client.page.scss'],
})
export class AddClientPage implements OnInit {
  livreur:any={
    "description": "",
    "email": "",
    "id": 0,
    "matricule": "",
    "nomComplet": "",
    "password": "",
    "tel": ""
  };
  constructor(public userService: UsersService,public storage: Storage,private router: Router) { }

  ngOnInit() {
  }

  addLiv() {
        console.log(this.livreur);
        this.userService.ajoutClient(this.livreur).subscribe(response=>{
          if(response){
            this.storage.set("user",response);
            this.router.navigate(['home']);
          }
          console.log(response);
        })
    }
}
