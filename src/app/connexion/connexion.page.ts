import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { LoadingController, ToastController } from '@ionic/angular';
import { UsersService } from '../api/users.service';

@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.page.html',
  styleUrls: ['./connexion.page.scss'],
})
export class ConnexionPage implements OnInit {
  telephone: any = "";
  pwd: any = "";
  erroridentifiant: boolean = false;
  emptyform: boolean = false;
  nom: any = '';
  email: string = '';
  nometat: boolean = false;
  telephoneetat: boolean = false;
  emailetat: boolean = false;
  pwdetat: boolean = false;
  constructor(private router: Router, private storage: Storage,
    public loadingController: LoadingController,
    private serviceClient: UsersService,
    private toastController: ToastController) {

  }

  ngOnInit() {
  }
  async connecter() {
    if (this.email == '') {
      this.telephoneetat = true;
    } else if (this.pwd == '') {
      this.pwdetat = true;
    } else {
      const loading = await this.loadingController.create({
        spinner: 'bubbles',
        message: 'En cours...',
        translucent: true,
        cssClass: 'custom-class custom-loading'
      });
      loading.present();
      /* var value = {
         "nom": this.nom, 
         "telephone": this.telephone, 
         "email": this.email, 
         "motDePasse": this.pwd
       }*/
      var user = {
        "username": this.email,
        "password": this.pwd
      };

      this.serviceClient.connection(user).subscribe(resp => {
        if (resp) {
          loading.dismiss();
          if (resp) {
            this.storage.set('identifiants',user);
            this.router.navigate(['home']);
          } else {
            this.erroridentifiant = true
          }
        } else {
          console.log();
        }
      }, error => {
        loading.dismiss();
      })
    }
  }
  async presentToast(pseudo) {
    const toast = await this.toastController.create({
      message: '' + pseudo,
      duration: 2000
    });
    toast.present();
  }

  hidemessage() {
    this.nometat = false;
    this.telephoneetat = false;
    this.emailetat = false;
    this.pwdetat = false;
    this.erroridentifiant = false;
  }
}
