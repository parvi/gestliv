import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';

// @ts-ignore
import { AgmCoreModule } from '@agm/core';
import { HttpClientModule } from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage';
import { TrajetService } from './api/trajet.service';
import { UsersService } from './api/users.service';
import { MainServicesService } from './api/main-services.service';
import { NotificationsService } from './notifications.service';
import { Network } from '@ionic-native/network/ngx';

import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { LoadingctrlService } from './loadingctrl.service';
const   firebase = {
  apiKey: "AIzaSyBAA6qZUTTFWFDYoO-KrH2NHpbj-qNV42g",
  authDomain: "dht-htsoft.firebaseapp.com",
  databaseURL: "https://dht-htsoft.firebaseio.com",
  projectId: "dht-htsoft",
  storageBucket: "gs://dht-htsoft.appspot.com/",
  messagingSenderId: "133539155181"
}
@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    IonicStorageModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBAA6qZUTTFWFDYoO-KrH2NHpbj-qNV42g'
    }),
    AppRoutingModule,
    HttpClientModule,
    AngularFireModule.initializeApp(firebase),
    AngularFirestoreModule,
    AngularFireAuthModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    AndroidPermissions,
    Geolocation,
    LocationAccuracy,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    TrajetService,
    UsersService,
    MainServicesService,
    NotificationsService,
    Network,
    LoadingctrlService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
