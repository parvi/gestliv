import { Injectable } from '@angular/core';
import { AlertController, ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class NotificationsService {

  constructor(public alertCtrl : AlertController, public toast: ToastController) { }

  
async presentConfirm(header: any,message: any,cancelText: any,okText: any): Promise<any> {
  return new Promise(async (resolve) => {
    const alert = await this.alertCtrl.create({
      header: header,
      message: message,
      buttons: [
        {
          text: cancelText,
          role: 'cancel',
          cssClass: 'secondary',
          handler: (cancel) => {
            resolve('cancel');
          }
        }, {
          text: okText,
          handler: (ok) => {
            resolve('ok');
          }
        }
      ]
    });
    alert.present();
  });
}

async presentToastMessage(message: string) {
  const toast = await this.toast.create({
    header: 'Toast header',
    message: message,
    position: 'top',
    buttons: [
      {
        side: 'start',
        icon: 'star',
        text: 'Favorite',
        handler: () => {
          console.log('Favorite clicked');
        }
      }, {
        text: 'Done',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }
    ]
  });
  toast.present();
}
}
