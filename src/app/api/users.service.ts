import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MainServicesService } from './main-services.service';

@Injectable()
export class UsersService {

  constructor(public http: HttpClient,public mainService: MainServicesService) { }
  //connection
  ajoutClient(value: any) {
    return this.http.post(this.mainService._url + '/clients', value);
  }
  connection(value : any){
    return this.http.post(this.mainService._urlLogin + '/login', value, {observe:'response'});
  }
  GetUserInfo(value : any){
    return this.http.get(this.mainService._url + '/livreursbysername'+value);
  }
}
