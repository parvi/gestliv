import { Injectable } from '@angular/core';
import { MainServicesService } from './main-services.service';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class TrajetService {
value= {
  idClient: "id",
  etat: "etat"
}
  constructor(public mainservice: MainServicesService, public http: HttpClient) {

   }
   ajoutTrajet(value: any) {
    return this.http.post(this.mainservice._url + '/trajets', value);
  }
  trajetAlivrer() {
    return this.http.get(this.mainservice._url + '/trajets/alivrer');
  }
  mesTrajetTerminer(){
    return this.http.get(this.mainservice._url + '/trajets/alivrer');
  }

  updateTrajet(value: any) {
    return this.http.post(this.mainservice._url + '/trajets/update/', value);
  }

}
