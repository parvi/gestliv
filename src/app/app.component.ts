import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  public appPages = [
    {
      title: 'Home',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'Mes courses(Client)',
      url: '/list',
      icon: 'list'
    },
      {
          title: 'Inscription Client',
          url: '/add-client',
          icon: 'list'
      }
    ,
    {
      title: 'Ajouter Course',
      url: '/add-course',
      icon: 'list'
    }
    ,
    {
      title: 'GPS POSITION',
      url: '/tracker-test',
      icon: 'home'
    },
    {
      title: 'Mes courses(Livreur)',
      url: '/choisir-trajet',
      icon: 'send'
    }
  ];
  user:any = null;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private storage: Storage,
    private router: Router
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.rootPage();
    });
  }
  rootPage(){
    this.storage.get("user").then((user)=>{
      console.log(user)
      if(user!=null){
        this.user = user;
        this.router.navigate(['home']);
      }else{
        this.router.navigate(['connexion']);
      }
    })
    
  }
}
