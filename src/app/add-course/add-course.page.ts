import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { TrajetService } from '../api/trajet.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { NotificationsService } from '../notifications.service';
import { LoadingctrlService } from '../loadingctrl.service';


@Component({
  selector: 'app-add-course',
  templateUrl: './add-course.page.html',
  styleUrls: ['./add-course.page.scss'],
})
export class AddCoursePage implements OnInit {
  trajet:any;
  locationCoords:any;
  constructor(private storage: Storage,
              private serviceTrajet: TrajetService,
              private geolocation: Geolocation,
              private notifs: NotificationsService,
              private load: LoadingctrlService) { 
    this.locationCoords = {
      latitude: "",
      longitude: "",
      accuracy: "",
      timestamp: ""
    }
  }

  ngOnInit() {
    this.initVar();
  }

    searchDestin() {

    }

  hidemessage() {

  }

 async validerExpedition() { 
    let load = await  this.load.presentLoading();
    this.storage.get("user").then((user)=>{
      this.trajet.idclient = user.id
      console.log(this.trajet);
      this.trajet.lalitude =  this.locationCoords.latitude;
      this.trajet.longitude =  this.locationCoords.longitude;
      this.serviceTrajet.ajoutTrajet(this.trajet).
      subscribe(response=>{
        load.onDidDismiss();
        this.initVar();
        this.notifs.presentToastMessage("Reussi");
        console.log(response);
      })
    });
  }
 async getLocationCoordinates() {
    let load = await  this.load.presentLoading();
    this.geolocation.getCurrentPosition().then((resp) => {
      load.onDidDismiss();
      this.locationCoords.latitude = resp.coords.latitude;
      this.locationCoords.longitude = resp.coords.longitude;
      this.locationCoords.accuracy = resp.coords.accuracy;
      this.locationCoords.timestamp = resp.timestamp;
      console.log(this.locationCoords);
    }).catch((error) => {
      load.onDidDismiss();
      alert('Error getting location' + error);
    });
  }
  initVar(){
   this.trajet = {
      "clientConfirmation": 0,
      "datecmd": new Date(),
      "dateliv": "",
      "descolis": "",
      "etat": 0,
      "idclient": 0,
      "idlivreur": 0,
      "lalitude": "",
      "lieuc": "",
      "lieud": "",
      "longitude": "",
      "montant": 0,
      "nomc": "",
      "tel": ""
    }
  }
}
