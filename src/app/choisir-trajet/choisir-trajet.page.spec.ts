import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChoisirTrajetPage } from './choisir-trajet.page';

describe('ChoisirTrajetPage', () => {
  let component: ChoisirTrajetPage;
  let fixture: ComponentFixture<ChoisirTrajetPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChoisirTrajetPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChoisirTrajetPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
