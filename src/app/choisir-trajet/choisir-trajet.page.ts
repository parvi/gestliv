import { Component, OnInit } from '@angular/core';
import { TrajetService } from '../api/trajet.service';
import { NotificationsService } from '../notifications.service';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';


@Component({
  selector: 'app-choisir-trajet',
  templateUrl: './choisir-trajet.page.html',
  styleUrls: ['./choisir-trajet.page.scss'],
})
export class ChoisirTrajetPage implements OnInit {
  trajet:any;
  constructor(public servicesTrajet: TrajetService,
           public servicesNotification: NotificationsService,
           public storage: Storage,
           public route: Router) {
    this.trajetAlivrer();
   }

  ngOnInit() {
  }
  trajetAlivrer(){
    console.log("suis la")
    this.servicesTrajet.trajetAlivrer()
    .subscribe(reponse=>{
      this.trajet = reponse;
      console.log(this.trajet);
    })
  }

 choisirCourse(item){
   var title = "Course";
   var message = "Voullez-vous accepter cette course"
  this.servicesNotification.presentConfirm(title, message, 'Non', 'OUI').then(res => {
    if (res === 'ok') {
      this.storage.get("user").then(livreur=>{
        item.etat = 1;
        item.livreur = livreur.id;
        this.servicesTrajet.updateTrajet(item)
        .subscribe(reponse=>{
          if(reponse){
            console.log(reponse)
            this.storage.set('courseencours',item);
            this.route.navigate(['encours-trajet']);
          }
        })
      })
     
    
     
    } else {

    }
  });
 }
}
